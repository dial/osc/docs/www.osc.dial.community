---
layout: page
title: "OSC Strategic Grant Program - Enterprise-Level Quality Improvements"
---

## Annex 4 - Product Consolidation

### Objective

DIAL has identified a critical challenge for free & open source software (FOSS) projects serving the sector: fragmented funding, siloed projects and organizations, and a general lack of awareness of existing products has led to duplicative efforts and an ecosystem of open source projects with overlapping capabilities. These products have limited resources to sustain themselves, both financial and human. This presents an opportunity for disparate communities of product maintainers to combine efforts and product functionality to produce a single product with a broader maintainer base, strongest features, and furthest potential reach.

### Scope of Work

This grant aims to provide a path for product consolidation in the FOSS ecosystem between two overlapping projects. Applicants should be project decision-makers who can build a roadmap involving documentation, feature comparisons, task breakdown, final execution of codebase merging, end-of-life plans for the “absorbed” project, and a strategy for assimilation into the “host” contributor community.

[Back to Strategic Grant Program Overview](stratagrant.html)
