---
layout: page
title: "OSC Strategic Grant Program - Managing Upstream Dependencies and Downstream Forks"
---

## Annex 5 - Managing Upstream Dependencies and Downstream Forks

### Objective

DIAL has identified a critical challenge for free & open source software (FOSS) projects serving the sector: there is a mis-match between the fast pace of programmatic work when deploying digital technologies “in the field” and the slower, more cautious work to update the project to a newer version of an underlying framework, or to incorporate new features into the core of an open source product’s codebase.

As a result, customizations that must be written with programmatic deadlines in mind are often never reincorporated as reusable features into the core product, leading to an ecosystem of custom “forks” of open source products all with single deployments, and a less feature-rich core product that has to be re-customized with each new programmatic use, leading to wasted and/or duplicative effort.

### Scope of Work

This grant aims to provide a path for reincorporation of programmatic software customization work into a product’s main codebase. Applicants should be T4D practitioners in the process of deploying a digital open source technology where customization work will be required. They should be able to product functional specifications for the new development work, as well as a timeline of when various features can begin to be reincorporated.

Example projects that would be within scope for this grant might include:
* In-kind support in the form of a DIAL software engineer, on secondment, to provide support for re-incorporating newly developed functionality into the core codebase of an open source product. This could include writing documentation, outreach to the core developer community, code cleanup and generalization, etc. as needed to successfully merge the code into code and synchronize with the on-location instance.
* Development of a modular architecture to allow truly custom work to be easily separated from reusable functionality which can be merged into the core.

[Back to Strategic Grant Program Overview](stratagrant.html)
