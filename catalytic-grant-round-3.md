---
layout: page
title: "OSC Catalytic Grant Program - Round 3"
---

_Applications for OSC Catalytic Grants Round 3 have closed and are being reviewed. Grant recipients will be contacted starting in early May 2019._

## About the program

The DIAL Open Source Center's Catalytic Grant program offers financial support for free & open source software projects that support work at the nexus of humanitarian action, development and peace. These grants are intended to support types of effort that have traditionally been neglected or unfinished by these software projects for various reasons.

**In general, we fund projects, features or efforts that are well-aligned with the [Principles for Digital Development](https://digitalprinciples.org/) and the [mission statement of the OSC](mission.html).** In order to extend our mission, accepted projects should be able to demonstrate a clear connection to how their work ultimately improves the lives of people and communities in low and middle-income countries, or those projects should demonstrate establishing such support through the work done via requested grant funds.

We currently plan to continue running this program two times each year, with applications opening each March and September.

Please read the information on this page carefully. If the grant sounds appropriate for your project, we strongly encourage you to apply!

### Key dates for this round

- **11 March 2019:** Program announced, proposals accepted
- **15 April 2019:** Deadline for applicants to submit proposals
- **Beginning of May 2019:** Notification of grant recipients

### Minimum criteria for applicant projects:

- Must be an existing free and open source software project; ideas and work still in a conceptual phase and/or without any existing published code may not apply.
- Must be collaborative with multiple individuals and/or organizations working collectively on the project.
- Must have multiple stakeholders; projects currently having only a single consumer individual or organization may not apply.
- Must be licensed under an [OSI approved](https://opensource.org/licenses) or [FSF free](https://www.gnu.org/licenses/license-list.html) software license; proprietary software projects or projects with additional restrictions contrary to the [Open Source Definition](https://opensource.org/osd-annotated) or [Free Software Definition](https://www.gnu.org/philosophy/free-sw.en.html) may not apply.
- Preference in selecting awardees will be given to organizations outside the United States and women-owned. For organizations that are primarily women led, a brief explanation may be submitted for consideration.

### Types of work we generally prioritize:

- Projects with wider scope (users, purpose, contributor/maintainer organizations) will be favored over projects with narrower scope.
- Efforts on to improve existing projects.
- Proposals to start new multi-stakeholder collaboration for new features or strategies of existing projects.
- Proposals to re-invigorating dormant projects of high potential value.

## Current Grant Round Details & Priorities

In this round of prioritized funding, DIAL will issue up to 4 grants, each up to $25,000 USD, to advance the OSC's mission of fostering healthy, sustainable open source communities and products. For the best likelihood of funding, we strongly encourage proposals to focus on one of the following priority areas:

### Priority A: Privacy & Responsible Data

- Development of features to support user compliance with regulatory requirements such as GDPR
- Features to allow/facilitate anonymization and/or deletion of user-owned content/data
- Improvements to underlying encryption, audit logging, and other security features

### Priority B: Improving user experiences

- Improved installation & configuration process for new deployments
- Field-based or laboratory research on user experience challenges faced by product users, admins, etc.
- Development/user testing of UI prototypes to improve known areas of user confusion/frustration, workflow review by UX designers, etc.

### Priority C: "Dirty Jobs"

- Improvements to the software development process, such as CI infrastructure, increasing test coverage, or technical backlog grooming
- Improvements to community and contribution, such as improved/consolidated documentation, better packaging, or training materials
- Improvements to the software code quality, such as refactoring code small or updating dependencies to newer versions


## Example past Catalytic Grant Recipients

For guidance about the scope of your proposal, it may be helpful to learn more about past grant recipients.

### Humanitarian OpenStreetMap Team: Improving Core Tools

This grant to Humanitarian OpenStreetMap Team (HOT) provided resources for a technical community manager, who will create clearly defined pathways for volunteers to contribute code, content, and documentation to four core HOT tools: Tasking Manager, Export Tool, OSM Analytics, and OpenAerialMap. A challenge HOT saw over the past few years ws engaging and on-boarding volunteers to contribute to non-mapping tasks like supporting marketing, writing content, or developing code for HOT’s tools. With the OSC's support, they plan to create the processes, guidelines, and infrastructure to enable volunteers to easily onboard and engage.

### OpenDataKit: Evolving the XLSForm & XForm Specifications

The Open Data Kit (ODK) ecosystem is tied together by two form specifications: the ODK XForms and XLSForm specifications. Enketo LLC previously worked on improving documentation for both. With this grant, Enketo will lead work on the current issue backlog, and define and propose processes for further evolution of the specifications. This work benefits all members of the ODK community at large, by making it easier for developers to build even more tools that will grow the ecosystem, enabling greater interoperability among those tools, and allowing for new features that are more well-considered. Despite being deeply important, this work had unfortunately been overlooked because it was cross-cutting and not explicitly tied to a specific project.

### LibreHealth: Containerization of LibreHealth EHR

The LibreHealth EHR platform is a large application that is used and frequently distributed as a full web stack installation. Installation and setup typically requires a technically savvy user to install all the dependencies such as an operating system, database server, web server, etc. While there have been both prebuilt VM distributions as well as Windows XAMPP style bundles, these are not typically easy to secure, resilient and scalable. This project involves rework on the core structure of the platform so that it can be deployed as a series of containers using common container management standards, such as Kubernetes.

## How to apply

Project leaders should coordinate applications within your open source community, and submit only one single proposal per open source project, representing your community's best idea. Applicants must submit the proposal form on this web site during the proposal window.

Applications must be completed on or before by **15 April 2019** [AOE/UTC-12](https://en.wikipedia.org/wiki/Anywhere_on_Earth). Please do not wait until the last minute to apply.

### Proposal review process

1. Your proposal will be reviewed by the OSC Governance Advisory Board's grant review committee, to ensure (a) the basic proposal criteria are included, and (b) your proposal fits the criteria and goals for the program. The reviewers will assess your project to understand its current stage of maturity, practices, and impact, and will write a short narrative evaluation with recommendations to the grant's sponsor.

2. The review committee will forward all reviews and final recommendations for this round to the sponsor funding panel, which for this round consists of DIAL management.

3. Depending on the funds requested, and the complexity of the work to be performed, the review committee and/or the sponsor funding panel may seek independent expert review of the proposal. (For example, if the project is focused on health, feedback might be sought from members of the OSC Health Sustainability Advisory Group, or externally our service delivery partners at Digital Square.) If those external reviews raise additional questions, applicants are given the opportunity to respond in writing to reviewer comments & questions.

4. Recipients will be notified at the end of the review cycle in early May 2019.

### Additional information needed from recipients

Selected recipients should be prepared to provide additional information to complete the grant process through our partners at the United Nations Foundation. Specific details will be provided to selected applicants as necessary. This information may include, but may not be limited to:

- DIAL Grant Recipient Assessment form,
- Internal Revenue Service forms, such as [W-9](https://www.irs.gov/pub/irs-pdf/fw9.pdf), [W-8BEN](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf), or [Form 8233](https://www.irs.gov/pub/irs-pdf/f8233.pdf), or similar,
- Basic line-item budget of activities funded by the grant, and
- [DUNS (Data Universal Numbering System) number](https://www.grants.gov/web/grants/applicants/organization-registration/step-1-obtain-duns-number.html), if available.

## Questions about the grant program?

If you have any questions about this grant program, its goals, requirements, or the application process, please [visit the Open Source Center Forum at https://forum.osc.dial.community and post your question](https://forum.osc.dial.community). We will strive to provide initial answers to all questions within 72 hours. **We encourage you to submit your questions no later than 1 April, to allow time for a thoughtful proposal.**

<p>
  <a href="services.html"><span class="icon fa-long-arrow-left"></span> Receive Services</a> |
  <a href="job-opportunity-program-lead.html">Work With Us<span class="icon fa-long-arrow-right">
<p>
