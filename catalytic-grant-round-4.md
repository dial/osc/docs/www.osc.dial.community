---
layout: page
title: "OSC Catalytic Grant Program - Round 4"
---

_APPLICATIONS CLOSED: The extended deadline to apply for OSC Catalytic Grant Round 4 has passed. Please make sure to [register on the OSC Hub](https://hub.osc.dial.community) for future updates._

## About the program

The [Digital Impact Alliance (DIAL)](https://digitalimpactalliance.org/) is pleased to sponsor this round of the Open Source Center's Catalytic Grant program, which offers financial support for free & open source software projects that support work at the nexus of humanitarian action, development and peace. These grants are intended to support types of effort that have traditionally been neglected or unfinished by these software projects.

In general, we fund projects, features or efforts that are well-aligned with the [Principles for Digital Development](https://digitalprinciples.org/) and the [mission statement of the OSC](mission.html). In order to extend our mission, accepted projects should be able to demonstrate a clear connection to how their work ultimately improves the lives of people and communities in low and middle-income countries, or those projects should demonstrate establishing such support through the work done via requested grant funds.

**Please read the information on this page carefully. If the grant sounds appropriate for your project, we strongly encourage you to apply!** We currently plan to continue offering financial assistance opportunities for free and open source software projects aligned with our mission, so if the timing or scope of this round does not work for your project, please stay in touch for future announcements. 

## On this page:

- [Round 4: Key dates and information](#round-4-key-dates-and-information)
  - [Minimum criteria for applicant projects](#minimum-criteria-for-applicant-projects)
  - [Types of work we generally prioritize](#types-of-work-we-generally-prioritize)

- [Round 4: Assistance offerings](#round-4-assistance-offerings)
  - [Sustainability planning](#sustainability-planning)
  - [Interoperability architecture planning](#interoperability-architecture-planning)
  - [Interoperability software engineering](#interoperability-software-engineering)

- [How to apply](#how-to-apply)
  - [Proposal review process](#proposal-review-process)
  - [Additional information needed from recipients](#additional-information-needed-from-recipients)
  - [Example past catalytic grant recipients](#example-past-catalytic-grant-recipients)
  - [Questions and answers](#questions-and-answers)

## Round 4: Key dates and information

- **14 January 2020:** Program announced, proposals accepted
- **EXTENDED through 29 February 2020:** (Was 23 February) Deadline for applicants to submit proposals
- **Beginning of April 2020:** Notification of grant recipients

### Minimum criteria for applicant projects

- Must be an existing free and open source software project; ideas and work still in a conceptual phase and/or without any existing published code may not apply.
- Must be collaborative with multiple individuals and/or organizations working collectively on the project.
- Must have multiple stakeholders; projects currently having only a single consumer individual or organization may not apply.
- Must be licensed under an [OSI approved](https://opensource.org/licenses) or [FSF free](https://www.gnu.org/licenses/license-list.html) software license; proprietary software projects or projects with additional restrictions contrary to the [Open Source Definition](https://opensource.org/osd-annotated) or [Free Software Definition](https://www.gnu.org/philosophy/free-sw.en.html) may not apply.

### Types of work we generally prioritize

- We particularly welcome applications from organizations and projects headquartered outside the United States, and those that are owned or led by women (as evidenced by a brief explanatory statement).
- Projects with wider scope and impact (users, purpose, contributor/maintainer organizations) will be favored over projects with narrower scope.
- Proposals to start new multi-stakeholder collaboration for new features or strategies of existing projects.
- Proposals for re-invigorating dormant projects of high potential value.
- Efforts to improve existing projects.

## Round 4: Assistance offerings

In this round of prioritized funding, DIAL will issue up to 5 grants, each valued up to $15,000 USD, to advance the OSC's mission of fostering healthy, sustainable open source communities and products. Our themes for this round are long-term sustainability planning for projects, and also technology interoperability (connecting data managed by your project's installations to other solutions for increased impact). In your application, you will need to choose one of the following options:

### Sustainability planning

- Access to the OSC's lightweight sustainability assessment and plan for your project. The objective of this consultation is a to provide specific and purposeful support for projects to achieve tangible outcomes that lead to increased maturity, scalability, and sustainability.
- Deliverables include a maturity assessment scorecard outlining the project's status across technical, legal, community, and sustainability dimensions; an impact/sustainability matrix describing specifical prioritized potential revenue streams for the project; a light financial model; and recommendations for future sustainability projects for the community.
- This grant includes 50 hours of consulting time from OSC experts in preparation of the above items, and $2,5000 USD to compensate for your project's partipation in interviews and/or exercises. The total value of the grant is $15,000 USD.

### Interoperability architecture planning

- This grant option is intended for projects that have not yet determined any strategy for interoperability with other tech products. 
- It offers a combination of in-kind technical architecture & design services from OSC technical teams, and optionally cash to compensate existing members of your community with necessary skills to work on this architecture, such as APIs, data format standards, etc. Building these solutions often involve significant software engineering efforts above and beyond the typical work done by an open source community.
- Combined cash and in-kind value can be up to $15,000 USD, as requested by the applicant.
- We encourage applicant projects to apply in partnership with one or more additional projects or platforms, to provide "real world" use cases for the design process. Beyond related open source products, this could also include third parties to assist with successfully sharing data between tools or platforms -- for example, [Open Function (OpenFn)](https://www.openfn.org/) as a data integration platform, or the [Fast Healthcare Interoperability Resources (FHIR)](http://fhir.org/) community for efforts on data standards.

### Interoperability software engineering

- This grant option is intended for projects that have already established a strategy for interoperability, such as APIs, data format standards, etc. Building these solutions often involve significant software engineering efforts above and beyond the typical work done by an open source community.
- It offers a combination of cash to compensate existing members of your community to deliver this functionality, and in-kind software engineering efforts from OSC technical teams.
- Combined cash and in-kind value can be up to $15,000 USD, as requested by the applicant.
- We encourage applicant projects to apply in partnership with one or more additional projects or platforms, to ensure there are one or more reference solutions as part of this project's efforts. Beyond related open source products, this could also include third parties to assist with successfully sharing data between tools or platforms -- for example, [Open Function (OpenFn)](https://www.openfn.org/) as a data integration platform, or the [Fast Healthcare Interoperability Resources (FHIR)](http://fhir.org/) community for efforts on data standards.

## How to apply

Project leaders should coordinate applications within your open source community, and submit only one single proposal per open source project, representing your community's best idea. Applicants must submit the proposal form on this web site during the proposal window.

[Click here to submit your application during the open period.](https://opensourcecenter.limequery.com/252232) Applications **must** be completed on or before by **the EXTENDED deadline of 29 February 2020** [AOE/UTC-12](https://en.wikipedia.org/wiki/Anywhere_on_Earth). (Original due date was 23 February.) You can save your work and come back to the application, using the links at the top of the application. Do not fail to submit your application before the deadline. **Late applications will not be accepted.**

### Proposal review process

1. Your proposal will be reviewed by the OSC Governance Advisory Board's grant review committee, to ensure (a) the basic proposal criteria are included, and (b) your proposal fits the criteria and goals for the program. The reviewers will assess your project to understand its current stage of maturity, practices, and impact, and will write a short narrative evaluation with recommendations to the grant's sponsor.

2. The review committee will forward all reviews and final recommendations for this round to the sponsor funding panel, which for this round consists of DIAL management.

3. Depending on the funds requested, and the complexity of the work to be performed, the review committee and/or the sponsor funding panel may seek independent expert review of the proposal. (For example, if the project is focused on health, feedback might be sought from members of the OSC Health Sustainability Advisory Group, or externally our service delivery partners at Digital Square.) If those external reviews raise additional questions, applicants are given the opportunity to respond in writing to reviewer comments & questions.

4. Recipients will be notified at the end of the review cycle in **early April 2020**.

### Additional information needed from recipients

Selected recipients should be prepared to provide additional information to complete the grant process through our partners at the United Nations Foundation. Specific details will be provided to selected applicants as necessary. This information may include, but may not be limited to:

- DIAL Grant Recipient Assessment form,
- Internal Revenue Service forms, such as [W-9](https://www.irs.gov/pub/irs-pdf/fw9.pdf), [W-8BEN](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf), or [Form 8233](https://www.irs.gov/pub/irs-pdf/f8233.pdf), or similar,
- Basic line-item budget of activities funded by the grant, and
- [DUNS (Data Universal Numbering System) number](https://www.grants.gov/web/grants/applicants/organization-registration/step-1-obtain-duns-number.html), if available.

### Example past catalytic grant recipients

For guidance about the scope of your proposal, it may be helpful to [learn more about some past grant recipients, available on our previous grant round's application page.](catalytic-grant-round-3.html#example-past-catalytic-grant-recipients)

### Questions and answers

If you have any questions about this grant program, its goals, requirements, or the application process, please [send an email with your question](mailto:grants@dial.community). Please refrain from sending confidential or private information via this method, as we intend to keep this page updated with questions and answers. We will strive to provide initial answers to all submitted questions within 72 hours. **We encourage you to submit your questions no later than 3 February 2020, to allow time for a thoughtful proposal.**

Please visit [https://hub.osc.dial.community/cg-04](https://hub.osc.dial.community/cg-04) for an up-to-date list of submitted questions and answers, or for an alternate means to submit questions.

<p>
  <a href="services.html"><span class="icon fa-long-arrow-left"></span> Receive Services</a> |
  <a href="job-opportunity-program-lead.html">Work With Us<span class="icon fa-long-arrow-right">
<p>
